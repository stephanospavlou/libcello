/*
 *  cello.c is a part of libcello.
 *
 *  libcello is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cello.h"
#include "hashmap.h"
#include "log.h"

#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memcpy()
#include <sys/random.h>

cello_list_t *cello_list_add(cello_list_t *list, void *data)
{
    if (!list) {
        list = malloc(sizeof(cello_list_t));
        list->data = data;
        list->next = NULL;
        list->prev = NULL;
        return list;
    }

    cello_list_t *node = malloc(sizeof(cello_list_t));
    node->data = data;
    node->next = NULL;
    node->prev = NULL;

    for (cello_list_t *i = list; i; i = i->next) {
        if (i->next == NULL) {
            node->prev = i;
            i->next = node;
        }
    }

    return list;
}

cello_list_t *cello_list_append_list(cello_list_t *list, cello_list_t *append)
{
    if (!list || !append) {
        _cellog_warn("cello_list_append_list(): parameter is NULL");
        return list;
    }

    for (cello_list_t *i = list; i; i = i->next) {
        if (i->next == NULL) {
            i->next = append;
            append->prev = i;
        }
    }

    return list;
}

void cello_list_remove(cello_list_t *haystack, void *needle,
    int (*cmp)(void *d1, void *d2))
{
    if (!haystack || !needle || !cmp) {
        _cellog_warn("cello_list_remove(): NULL parameter\n");
        return;
    }

    for (cello_list_t *i = haystack; i; i = i->next) {
        if (cmp(needle, i->data)) {
            if (i->next && i->prev) {
                i->prev->next = i->next;
                i->next->prev = i->prev;
                cello_list_free(i);
            } else if (i->next) {
                cello_list_t *tmp = i->next;
                i->next->prev = NULL;
                cello_list_free(i);
            } else if (i->prev) {
                cello_list_t *tmp = i->prev;
                i->prev->next = NULL;
                cello_list_free(i);
            } else {
                cello_list_free(i);
            }
        }
    }
}

cello_list_t *cello_list_dup(cello_list_t *list, ssize_t size)
{
    if (!list) {
        _cellog_warn("cello_list_dup(): NULL list");
        return list;
    }

    cello_list_t *dup = NULL;
    for (cello_list_t *i = list; i; i = i->next) {
        void *data = malloc(size);
        memcpy(data, i->data, size);
        dup = cello_list_add(dup, data);
    }
    return dup;
}

int cello_list_contains(cello_list_t *haystack, void *needle, 
    int (*cmp)(void *d1, void *d2))
{
    if (!haystack || !needle || !cmp) {
        _cellog_warn("cello_list_contains(): NULL parameter\n");
        return 0;
    }

    for (cello_list_t *i = haystack; i; i = i->next) {
        if (cmp(needle, i->data)) {
            return 1;
        }
    }
    return 0;
}

void cello_list_free(cello_list_t *list)
{
    if (!list) {
        _cellog_warn("cello_list_free(): list is NULL");
        return;
    }

    cello_list_t *i, *j;
    for (i = list; i; i = i->next) {
        j = i;
        if (j->prev) {
            free (j->prev);
        }
    }
    free(j);
}

static lattice_point_t* _transform_square(lattice_point_t *point)
{
    return point;
}

static lattice_point_t *(*_transform[])(lattice_point_t *) = {
    _transform_square
};

lattice_point_t *lattice_point_new() 
{
    lattice_point_t *point = malloc(sizeof(lattice_point_t));
    point->entries = NULL;
    return point;
}

lattice_point_t *lattice_point_from_entries(cello_list_t *entries)
{
    if (!entries) {
        _cellog_warn("lattice_point_from_entries(): entries is NULL");
        return lattice_point_new();
    }

    lattice_point_t *point = lattice_point_new();
    point->entries = cello_list_dup(entries, sizeof(unsigned int));

    return point;
}

lattice_point_t *lattice_point_dup(lattice_point_t *orig)
{
    return lattice_point_from_entries(orig->entries);
}

void lattice_point_replace(lattice_point_t *point, void *find,
    void *replace)
{
    for (cello_list_t *i = point->entries; i; i = i->next) {
        if (find == i->data) {
            free(i->data);
            i->data = replace;
            break;
        }
    }
}

void lattice_point_free(lattice_point_t *point) 
{
    cello_list_free(point->entries);
    free(point);
}

static int rand_initialized = 0;
static gmp_randstate_t randstate;

static unsigned long _rand_mt(int range)
{
    if (!rand_initialized) {
        void *buf = NULL;
        ssize_t buf_length = getrandom(buf, 5, 0);
        if (buf_length == -1) {
            _cellog_error("_random_lattice_point(): error generating random");
            _cellog_error(" number.\n");
            return range;
        }

        gmp_randinit_mt(randstate);
        gmp_randseed_ui(randstate, (unsigned long)buf);
        rand_initialized = 1;
    }

    return gmp_urandomm_ui(randstate, range);
}

lattice_point_t *rand_lattice_point_mt(lattice_t *lattice)
{
    if (!lattice) {
        _cellog_warn("rand_lattice_point_mt(): lattice is NULL\n");
        return NULL;
    }

    lattice_point_t *max_point = lattice->max_point;
    if (!max_point) {
        _cellog_warn("rand_lattice_point_mt(): max_point of"
            "lattice is NULL\n");
        return NULL;
    }

    lattice_point_t *rand_point = lattice_point_new();
    for (cello_list_t *i = max_point->entries; i; i = i->next) {
        unsigned int *entry = malloc(sizeof(unsigned int));
        *entry = _rand_mt(*(unsigned int*)i->data);

        if (*entry == *(unsigned int*)i->data) {
            _cellog_error("rand_lattice_point_mt(): return of _rand_mt()"
                " indicates failure. aborting random lattice point"
                "generation\n");
            return NULL;
        }

        cello_list_add(rand_point->entries, entry);
    }

    return _transform[lattice->type](rand_point);
}

cello_list_t *lattice_point_neighbors(lattice_t *lattice,
    lattice_point_t *bp)
{
    cello_list_t *neighbors = NULL;

    if (!lattice) {
        _cellog_warn("lattice_point_neighbors(): lattice is NULL\n");
        return NULL;
    }

    lattice_point_t *max_point = lattice->max_point;
    if (!max_point) {
        _cellog_warn("lattice_point_neighbors(): max_point of"
            "lattice is NULL\n");
        return NULL;
    }

    lattice_point_t *bp_dup = lattice_point_dup(bp);

    cello_list_t *i = bp_dup->entries, *j = max_point->entries;
    for (; i; i = i->next, j = j->next) {
        unsigned int bp_entry_val = *(unsigned int*)i->data;
        unsigned int max_point_entry_val = *(unsigned int*)j->data;

        if ((bp_entry_val + 1 <= max_point_entry_val) &&
            !(bp_entry_val + 1 >max_point_entry_val)) {
            unsigned int *entry = malloc(sizeof(unsigned int));
            *entry = bp_entry_val + 1;
            lattice_point_replace(bp_dup, i->data, entry);
            neighbors = cello_list_add(neighbors, bp_dup);
        }
        if ((bp_entry_val - 1 <= max_point_entry_val) &&
            !(bp_entry_val - 1 > max_point_entry_val)) {
            unsigned int *entry = malloc(sizeof(unsigned int));
            *entry = max_point_entry_val - 1;
            lattice_point_replace(bp_dup, i->data, entry);
            neighbors = cello_list_add(neighbors, bp_dup);
        }
    }

    return neighbors;
}

lattice_point_t *rand_lattice_point_neighbor_mt(lattice_t *lattice,
    lattice_point_t *bp)
{
    lattice_point_t *rand_neighbor = NULL;
    cello_list_t *neighbors = lattice_point_neighbors(lattice, bp);

    cello_list_t *i = neighbors;
    unsigned int neighbor_count = 0;
    for (; i; i = i->next, neighbor_count++);

    int random_neighbor = _rand_mt(neighbor_count);
    if (random_neighbor == neighbor_count) {
        _cellog_warn("rand_lattice_point_neighbor_mt(): return value of"
            " _rand_mt() indicates failure. aborting attempt to select random"
            " neighbor of lattice point");
        return NULL;
    }

    i = neighbors;
    for (int j = 0; j < neighbor_count; j++) {
        if (j == random_neighbor) {
            rand_neighbor = i->data;
            break;
        }
        i = i->next;
    }

    return rand_neighbor;
}

lattice_t *lattice_new(int dimension, lattice_type_e type,
    lattice_point_t *max_point)
{
    lattice_t *lattice = malloc(sizeof(lattice_t));
    lattice->dimension = dimension;
    lattice->type = type;
    lattice->max_point = max_point;

    return lattice;
}

void lattice_free(lattice_t *lattice) {
    free(lattice->max_point);
    free(lattice);
}

struct _cell_map_t {
    struct hashmap *map;
};

typedef struct _m_item {
    lattice_point_t *point;
    cell_t *cell;
} _m_item;

static uint64_t _hash_m_item(const void *item, uint64_t seed0, uint64_t seed1)
{
    const _m_item *i = (_m_item*)item;
    return hashmap_sip(i->point, sizeof(lattice_point_t*), seed0,
        seed1);
}

static int _cmp_m_item(const void *a, const void *b, void *udata)
{
    lattice_point_t *p1 = ((_m_item*)a)->point;
    lattice_point_t *p2 = ((_m_item*)b)->point;

    cello_list_t *i = p1->entries, *j = p2->entries;
    for (; i && j; i = i->next, j = j->next) {
        unsigned int* val1 = i->data;
        unsigned int* val2 = j->data;
        if (*val1 != *val2) {
            return 0;
        }
    }

    if (!i && !j) {
        return 0;
    } else {
        return 1;
    }
}

cell_map_t *cell_map_new()
{
    cell_map_t *map = malloc(sizeof(cell_map_t));
    map->map = hashmap_new(sizeof(_m_item), 0, 0, 0, _hash_m_item,
        _cmp_m_item, NULL, NULL);
    return map;
}

void cell_map_put(cell_map_t *map, lattice_point_t *point, cell_t *cell)
{
    hashmap_set(map->map, &(_m_item){.point = point, .cell = cell});
}

void cell_map_remove(cell_map_t *map, lattice_point_t *point)
{
    hashmap_delete(map->map, &(_m_item){.point = point});
}

cell_t *cell_map_get(cell_map_t *map, lattice_point_t *point)
{
    const _m_item *item = hashmap_get(map->map,
        &(_m_item){.point = point});
    
    if (!item) {
        return NULL;
    } else {
        return item->cell;
    }
}

int cell_map_contains_key(cell_map_t *map, lattice_point_t *point)
{
    return !cell_map_get(map, point) ? 1 : 0;
}

void cell_map_free(cell_map_t *map)
{
    hashmap_free(map->map);
    free(map);
}

lattice_point_t *standard_cpotts_model_sampling(cpotts_model_t *model)
{
    return rand_lattice_point_mt(model->lattice);
}

lattice_point_t *standard_cpotts_model_neighbor_sampling(cpotts_model_t *model,
    lattice_point_t *bp)
{
    return rand_lattice_point_neighbor_mt(model->lattice, bp);
}

int standard_cpotts_model_decide(cpotts_model_t *model, cell_t *bc, cell_t *nc)
{
    /* assuming first prop is cellid */
    unsigned int cellid1 = *(unsigned int*)bc->props->data;
    unsigned int cellid2 = *(unsigned int*)nc->props->data;

    int dec = cellid1 - cellid2;
    return dec >= 0 ? 0 : 1;
}

void standard_cpotts_model_mutate(cpotts_model_t *model, cell_t *bc, cell_t *nc,
    int dec)
{
    /* assuming first prop is cellid */
    if (dec) {
        unsigned int *cellid1 = (unsigned int*)bc->props->data;
        unsigned int *cellid2 = (unsigned int*)nc->props->data;

        *cellid1 = *cellid2;
    }
}

void standard_cpotts_model_step(cpotts_model_t *model)
{
    lattice_point_t *sampling = standard_cpotts_model_sampling(model);
    lattice_point_t *neighbor_sampling =
        standard_cpotts_model_neighbor_sampling(model, sampling);

    cell_t *bc = cell_map_get(model->cell_map, sampling);
    cell_t *nc = cell_map_get(model->cell_map, neighbor_sampling);

    int dec =standard_cpotts_model_decide(model, bc, nc);
    standard_cpotts_model_mutate(model, bc, nc, dec);
}

static cell_t *_standard_cpotts_model_cell_new() {
    cell_t *cell = malloc(sizeof(cell_t));

    unsigned int *id = malloc(sizeof(unsigned int));
    *id = _rand_mt(10);

    cello_list_t *props = NULL;
    props = cello_list_add(props, id);

    cell->props = props;
    return cell;
}

void standard_cpotts_model_rand_pop(cpotts_model_t *model) {
    for (int i = 0; i < 10; i++) {
        cell_map_put(model->cell_map, rand_lattice_point_mt(model->lattice),
            _standard_cpotts_model_cell_new());
    }
}

cpotts_model_t *standard_cpotts_model_new(void)
{
    cpotts_model_t *model = malloc(sizeof(cpotts_model_t));

    lattice_point_t *max_point = lattice_point_new();
    unsigned int *entry1 = malloc(sizeof(unsigned int));
    unsigned int *entry2 = malloc(sizeof(unsigned int));
    *entry1 = 20;
    *entry2 = 20;
    max_point->entries = cello_list_add(max_point->entries, entry1);
    max_point->entries = cello_list_add(max_point->entries, entry2);

    model->lattice = lattice_new(2, SQUARE_LATTICE, max_point);
    model->lattice = 0;
    model->cell_map = cell_map_new();
    standard_cpotts_model_rand_pop(model);
    
    model->sampling = standard_cpotts_model_sampling;
    model->neighbor_sampling = standard_cpotts_model_neighbor_sampling;
    model->decide = standard_cpotts_model_decide;
    model->mutate = standard_cpotts_model_mutate;
    model->step = standard_cpotts_model_step;

    return model;
}

void cpotts_model_free(cpotts_model_t *model)
{
    lattice_free(model->lattice);
    cell_map_free(model->cell_map);
    free(model);
}