/*
 *  log.h is a part of libcello.
 *
 *  libcello is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CELLOG_H
#define CELLOG_H

typedef enum _cellog_level_e {
    DEBUG,
    WARN,
    ERROR
} _cellog_level_e;

void _cellog_event(_cellog_level_e level, const char *fmt, ...);
void _cellog_debug(const char *fmt, ...);
void _cellog_warn(const char *fmt, ...);
void _cellog_error(const char *fmt, ...);

#endif