/*
 *  cello.h is a part of libcello.
 *
 *  libcello is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CELLO_H
#define CELLO_H

#include <stdio.h>

/* linked list node */
typedef struct _cello_list_t {
    void *data;
    struct _cello_list_t *prev;
    struct _cello_list_t *next;
} cello_list_t;

/**
 * Append node to list.
 * If list is NULL, a new list is created and returned
 * with the single node from above.
 * @param list list to append new node to
 * @param data data to be contained by new node appended
 * list
 * @return if list is NULL, a new cello_list_t, otherwise
 * the modified list
 */
cello_list_t *cello_list_add(cello_list_t *list, void *data);
/**
 * Append list to end of another list.
 * @param list list to append to
 * @param append list that should be appended to end of first
 * parameter
 */
cello_list_t *cello_list_append_list(cello_list_t *list, cello_list_t *append);
/**
 * Remove node from list.
 * @param list list to remove node from
 * @param needle node to remove from list
 * @param cmp pointer to compare function (used to check if a
 * given node is needle)
 */
void cello_list_remove(cello_list_t *list, void *needle,
    int (*cmp)(void *d1, void *d2));
/**
 * Duplicate list.
 * @param list the list to duplicate
 * @param size size of data stored in node
 * @return duplicated list
 */
cello_list_t *cello_list_dup(cello_list_t *list, ssize_t size);
/**
 * Check if list contains node.
 * @param list the list to search
 * @param needle node to search for
 * @param cmp pointer to compare function (used to check if a
 * given node is needle)
 * @return 1 if node is found, 0 otherwise
 */
int cello_list_contains(cello_list_t *list, void *needle, 
    int (*cmp)(void *d1, void *d2));
/**
 * Free memory allocated for list.
 * @param list list to free
 */
void cello_list_free(cello_list_t *list);

/* vector belonging to a lattice */
typedef struct _lattice_point_t {
    cello_list_t *entries;
} lattice_point_t;

/* lattice type, more types coming soon */
typedef enum _lattice_type_e {
    SQUARE_LATTICE
} lattice_type_e;

/* a libcello lattice is a linear transformation of Z^n, where
 * n is dimension and the linear transformation is specfied
 * by type. max point is used to create a finite subsection
 * of the lattice to use in a model. a given lattice point is
 * considered inside this lattice if entry_i of the point is
 * <= entry_i of max_point.
 */
typedef struct _lattice_t {
    unsigned int dimension;
    lattice_type_e type;
    lattice_point_t *max_point;
} lattice_t;

/**
 * Create a new lattice point.
 * @return a newly allocated lattice point
 */
lattice_point_t *lattice_point_new(void);
/**
 * Create a new lattice point from a list of point entries.
 * @param entries list of entries to copy into new lattice point
 * @return a newly allocated lattice point with copied entries list
 */
lattice_point_t *lattice_point_from_entries(cello_list_t *entries);
/**
 * Duplicate an existing lattice point.
 * @param orig original lattice point to copy
 * @return duplicated lattice point
 */
lattice_point_t *lattice_point_dup(lattice_point_t *orig);
/**
 * Replace an entry node in an existing lattice point.
 * @param point lattice point to modify
 * @param data entry node to replace
 * @param replace new entry node to replace with
 */
void lattice_point_replace(lattice_point_t *point, void *data,
    void *replace);
/**
 * Free allocated memory for lattice point.
 * @param point lattice point to free
 */
void lattice_point_free(lattice_point_t *point);

/**
 * Generate random lattice point within lattice.
 * This function uses the GMP implementation of the
 * Mersenne Twister PRNG to select a random lattice point.
 * @param lattice lattice random point should exist on
 * @return a random lattice point on the given lattice,
 * if lattice is NULL or max_point is NULL, or if error,
 * returns NULL
 */
lattice_point_t *rand_lattice_point_mt(lattice_t *lattice);
/**
 * Produce neighbors of given lattice point on given lattice.
 * @param lattice lattice bp exists on
 * @param bp point to find neighbors of
 * @return list of neighbors of bp on given lattice
 */
cello_list_t *lattice_point_neighbors(lattice_t *lattice,
    lattice_point_t *bp);
/**
 * Produce a single random neighbor of a point.
 * This function uses the GMP implementation of the
 * Mersenne Twister PRNG to select a random neighbor.
 * @param lattice lattice bp exists on
 * @param bp point to select neighbor of
 * @return a random neighbor of bp, if error returns NULL
 */
lattice_point_t *rand_lattice_point_neighbor_mt(lattice_t *lattice,
    lattice_point_t *bp);
/**
 * Create a new lattice.
 * Create a new lattice given a dimension, type and
 * max_point.
 * @param dimension dimension of lattice
 * @param type lattice type
 * @param max_point max_point defining boundary of lattice
 */
lattice_t *lattice_new(int dimension, lattice_type_e type,
    lattice_point_t *max_point);
/**
 * Free memory allocated for lattice.
 * @param lattice lattice to free
 */
void lattice_free(lattice_t *lattice);

/**
 * a libcell cell is simply a list of properties. what
 * properties are expected is left up the model implementation.
 */
typedef struct _cell_t {
    cello_list_t *props;
} cell_t;

/**
 * Wrapper struct around a hashmap, containing
 * (lattice_point_t, cell_t) key-value pairs.
 */
typedef struct _cell_map_t cell_map_t;

/**
 * Create new cell map.
 * @return a newly allocated cell map
 */
cell_map_t *cell_map_new(void);
/**
 * Add a new item to a cell map.
 * @param map map to add item to
 * @param */
void cell_map_put(cell_map_t *map, lattice_point_t *point, cell_t *cell);
/**
 * Remove an item from a cell map.
 * @param map map to remove item from
 * @param point remove cell at this point, if one exists
 */
void cell_map_remove(cell_map_t *map, lattice_point_t *point);
/**
 * Get cell from cell map that corresponds with given lattice point.
 * @param map map to get cell from
 * @param point lattice point that corresponds with desired cell
* @return cell if one is found, otherwise return NULL
 */
cell_t *cell_map_get(cell_map_t *map, lattice_point_t *point);
/**
 * Check if cell map contains a lattice point.
 * @param map map to search
 * @param point lattice point to search map for.
 * @return 1 if cell is lattice point is found, 0 otherwise
 **/
int cell_map_contains_point(cell_map_t *map, lattice_point_t *point);
/**
 * Free allocated memory for cell map.
 * @param map cell map to free
 */
void cell_map_free(cell_map_t *map);

/**
 * Container for Cellular Potts or Glazier-Graner-Hogeweg
 * model.
 */
typedef struct _cpotts_model_t {
    /* lattice the model will operate on */
    lattice_t *lattice;
    /* total run/current generation */
    unsigned int generation;

    /* list of cells. this should be a hash map */
    cell_map_t *cell_map;

    /* produce sampling of lattice_points */
    lattice_point_t *(*sampling)(struct _cpotts_model_t *model);
    /* given base point (bp), produce sampling of neighbors */
    lattice_point_t *(*neighbor_sampling)
        (struct _cpotts_model_t *model, lattice_point_t *bp);
    /* decide to mutate or not */
    int (*decide)(struct _cpotts_model_t *model, cell_t *bc, cell_t *nc);
    /* logic for cell mutation */
    void (*mutate) (struct _cpotts_model_t *model, cell_t *bc, cell_t *nc,
        int dec);
    /* evolve model */
    void (*step)(struct _cpotts_model_t *model);
} cpotts_model_t;

/**
 * Create new 'Standard' Cellular Potts model.
 * @return a newly allocated 'standard' Cellular Potts model
 */
cpotts_model_t *standard_cpotts_model_new(void);
/**
 * 'Standard' implementation of function 'sampling'.
 * Select a random lattice point from the lattice operated on
 * by the model.
 * @param model model to be sampled
 * @return a random lattice point on lattice operated on by the model,
 * if error returns NULL
 */
lattice_point_t *standard_cpotts_model_sampling(cpotts_model_t *model);
/**
 * 'Standard' implementation of function 'neighbor sampling'.
 * Select a random neighbor lattice point of given base point
 * from the lattice operated on by the model.
 * @param model model to be sampled
 * @param bp base point whose neighbors will be sampled
 * @return a random neighbor lattice point of base point on lattice
 * operated on by the model, if error returns NULL
 */
lattice_point_t *standard_cpotts_model_neighbor_sampling(cpotts_model_t *model,
    lattice_point_t *bp);
/**
 * 'Standard' implementation of function 'decide'.
 * Determine, given a base cell and a neighbor cell, whether or not
 * to mutate the base lattice point.
 *
 * @param model model to be operated on
 * @param bc base cell
 * @param nc neighbor cell
 * @return 1 if base cell should be mutated, 0 otherwise
 */
int standard_cpotts_model_decide(cpotts_model_t *model, cell_t *bc, cell_t *nc);
/**
 * 'Standard' implementation of function 'mutate'.
 * Mutate base cell with regards to neighbor cell if decision is 1.
 * @param model model to be operated on
 * @param bc base cell
 * @param nc neighbor cell
 * @param dec decision to mutate (1 for yes, 0 for no)
 */
void standard_cpotts_model_mutate(cpotts_model_t *model, cell_t *bc, cell_t *nc,
    int dec);
void standard_cpotts_model_step(cpotts_model_t *model);
/**
 * Free any allocated memory for Cellular Potts model.
 * @param model model to be freed
 */
void cpotts_model_free(cpotts_model_t *model);

#endif